/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package laberinto;

import java.io.BufferedReader;
import java.io.FileReader;

/**
 * Revisar figuras en https://en.wikipedia.org/wiki/Geometric_Shapes
 * @author Angélica Reyes Arellano
 */
public class Laberinto {

    // Para manejar mas facil las entradas
    private static final short PARED = 1;
    private static final short CAMINO = 0;
    private static final short INICIO = 2;
    private static final short FIN = 3;
    private static final short RECORRIDO = 4;  // Por donde es el camino

    /**
     * Numero de columnas del laberinto
     */
    private int dimX;

    /**
     * Numero de renglones del laberinto
     */
    private int dimY;

    //Coordenadas del inicio del laberinto
    private int inX;
    private int inY;
    private String origen; //Direccion de origen para entrar al laberinto

    /**
     * Matriz de enteros que representa al laberinto
     */
    public short[][] laberinto;

    /**
     * Constructor del laberinto
     * @param nomArch nombre del archivo de entrada a partir del cual se generara el laberinto
     */
    public Laberinto(String nomArch){
        leer(nomArch);
    }

    /**
     * Llena la matriz a partir del archivo especificado y determina las coordenadas
     * de inicio, así como la dirección de origen
     * @param nomArch Archivo de entrada para cargar el laberinto.
     */
    private void leer(String nomArch) {
        try{
            // Se hace la lectura
            FileReader fr = new FileReader(nomArch);
            BufferedReader br = new BufferedReader(fr);
            dimX = Integer.parseInt(br.readLine());  // Lee renglones en un String y lo pasa a int
            dimY = Integer.parseInt(br.readLine());  // Lee las columnas

            // Genera la matriz para posteriormente llenarla
            laberinto = new short[dimY][dimX];

            //llena
            for(int i = 0; i < dimY; i++){
                for(int j =0; j < dimX+1; j++){ //Mas 1 por el salto de linea
                    // Switch para llenar la matriz con el numero correcto
                    switch(br.read()){  //Se lee unicamente un caracter
                        case '0': 
                            laberinto[i][j] = CAMINO;
                            break;
                        case '1':
                            laberinto[i][j] = PARED;
                            break;
                        case 'S':
                            laberinto[i][j] = INICIO;
                            // Se guardan las coordenadas de inicio
                            inX = i;
                            inY = j;
                            // se determina el origen con if de una línea
                            origen = i == 0? "ar" : j== 0? "iz": j== dimX? "de" : "ab";
                            break;
                        case 'F':
                            laberinto[i][j] = FIN;
                            break;
                        default:
                            break;
                    }
                }
            }
        }catch(Exception e){
            System.err.println("Error en metodo leer con el archivo " + nomArch);
            e.printStackTrace();  // Muestra toda la excepcion y nos ayuda a identificar mejor el error
        }
    }

    /**
     * Determina que hay en la casilla en la direccion dada
     * @param posX Coordenada X desde donde se voltea a ver
     * @param posY Coordenada Y desde donde se voltea a ver
     * @param direccion Puede ser ar, ab, iz y der
     * @return Si es valida la casilla regresa CAMINO, PARED, FIN; -1 en otro caso.
     */
    private short explora(int posX, int posY, String direccion){
        // TAREA. Llenar los casos para que se regrese el valor adecuado al explorar
        short valor = -1; //
        try{
            switch(direccion){
                case "ar":
                    valor = laberinto[posX-1][posY];
                    break;
                case "ab":
                //Va a moverse hacia abajo
                    valor=laberinto[posX+1][posY];
                    break;
                case "iz":
                //Va a moverse hacia la izquierda
                    valor=laberinto[posX][posY-1];
                    break;
                case "de":
                //va a moverse hacia la derecha
                    valor=laberinto[posX][posY+1];
                    break;
            }
        }
        catch(Exception e){
            valor = -1;
            //e.printStackTrace();
        }
        return valor;
    }

    /**
     * Metodo principal que resuelve el laberinto.
     * Si hay solucion, reescribe la solución sobre la matriz e imprime el
     * laberinto resuleto, en caso contrario imprime que no hay solucion
     */
    public void resLab(){
        //System.out.println("inX:" + inX + " inY:" + inY + "INICIO");
        if (exploraRecursivo(inX,inY, origen)){  // Si devuelve verdadero, encontró solucion
            System.out.println("Solucion\n");
            print();
        }else{
            System.out.println("No hay solucion\n");
        }
    }

    /**
     * Metodo auxiliar a resLab. Explora recursivamente el laberinto. Cuando encuentra una salida
     * sobreescribe e camino por donde pasó y regresa verdadero.
     * @param coX Coordenada X de la casilla que esta explorando en la matriz
     * @param coY Coordenada Y de la casilla que esta explorando en la matriz
     * @param orig Direccion de donde viene la exploracion. Los valores validos son:
     * "ar", "ab", "iz" y "de" para arriba, abajo, izquierda y derecha, respectivamente.
     * @return Verdadero si al explorar en alguna dirección está la salida, false en 
     * caso contrario.
     */
    private Boolean exploraRecursivo(int coX, int coY, String orig) {
        // Tarea. Llenar las exploraciones a las demás direcciones
        short exp;
        // Exploracion arriba
        if( !orig.equals("ar")){
            //Busca y se va guardando el resultado
            exp = explora(coX, coY, "ar"); 
            if(exp == FIN){
                laberinto[coX][coY] = RECORRIDO;
                return true;
            }else if (exp == CAMINO){
                if(exploraRecursivo(coX-1, coY, "ab")){
                    laberinto[coX][coY] = RECORRIDO;
                    return true;
                }
            }
        }
        
        // Exploracion derecha
        if( !orig.equals("de")){
            //Busca y se va guardando el resultado
            exp = explora(coX, coY, "de"); 
            if(exp == FIN){
                laberinto[coX][coY]= RECORRIDO;
                return true;
            }
            
                        else if(exp==CAMINO){
                            if(exploraRecursivo(coX, coY+1, "iz")){
                                laberinto[coX][coY]=RECORRIDO;
                                   return true;
                }
            }
        }

        // Exploracion  abajo
        if( !orig.equals("ab")){
            //Busca y se va guardando el resultado
            exp=explora(coX, coY, "ab"); 
            if(exp == FIN){
                laberinto[coX][coY]=RECORRIDO;
                return true;
            }
            
                         else if(exp==CAMINO){
                                if(exploraRecursivo(coX+1, coY, "ar")){
                                    laberinto[coX][coY]=RECORRIDO;
                                        return true;
                }
            }
        }

        // Exploracion izquierda
        if( !orig.equals("iz")){
            //Busca y se va guardando el resultado
            exp=explora(coX, coY, "iz"); 
            if(exp==FIN){
                laberinto[coX][coY]=RECORRIDO;
                return true;
            }
                            else if(exp==CAMINO){
                                  if(exploraRecursivo(coX, coY-1, "de")){
                                        laberinto[coX][coY]=RECORRIDO;
                                        return true;
                }
            }
        }

        return false; //Todas las exploraciones fueron fallidas
    }

    /**
     * Imprime la matriz
     */
    public void print(){
        String pared = "\u25A0";
        String camino = "\u25A1";
        String inicio = "\u25BD";
        String fin = "\u25CB";
        String recorrido = "\u25C6";;  // buscar un caracter para el recorrido
        
        
        System.out.println("Pared: "+pared);
        System.out.println("Camino: "+camino);
        System.out.println("Inicio: "+inicio);
        System.out.println("Fin: "+fin);
        System.out.println("Recorrido: "+recorrido+"\n");
        
        for(int i = 0; i < dimY; i++){
            for(int j = 0; j < dimX; j++){
                
                if(laberinto[i][j] == PARED){
                    System.out.print(PARED);
                }
                if(laberinto[i][j]==INICIO){
                    System.out.print(INICIO);
                }
                if(laberinto[i][j]==CAMINO){
                    System.out.print(CAMINO);
                }
                if(laberinto[i][j]==RECORRIDO){
                    System.out.print(RECORRIDO);
                }
                if(laberinto[i][j] == FIN){
                    System.out.print(FIN);
                }
                
            }
            
            
            System.out.println();// Impresión para el siguiente renglon
        }
    }

    /**
     * Metodo main de la clase
     * @param args
     */
    public static void main (String[] args){
        Laberinto l = new Laberinto("labs/laberinto2.txt"); //genera un objeto con el archivo que contiene al laberinto
        
        l.print(); // imprime el laberinto recien leído
        l.resLab(); // resuelve el laberinto y lo imprime 
        
    }


}
